package; 

	import flixel.FlxSprite;
	import flixel.FlxObject;
	import flixel.text.FlxText;
	import flixel.FlxState;
	import flixel.FlxG;
	import flixel.group.FlxGroup;
//	import flixel.group.FlxTypedGroup;
	import flixel.util.FlxSort;
	import flixel.math.FlxMath;
	import flixel.util.FlxTimer;
	import flixel.math.FlxRandom;
	import flixel.tile.FlxTilemap;
	
	import flash.system.System;
	
	import flixel.input.gamepad.FlxGamepad;
	import flixel.input.gamepad.FlxGamepadInputID;
	
	class TitleState extends FlxState
	{
		
		private var ggj:FlxSprite;
		private var bgs:FlxSprite;
		private var title:FlxSprite;
		private var howtoplay:FlxSprite;
		
		private var introTimer:Float;
		
		private var _gamePads:Array<FlxGamepad> = new Array<FlxGamepad>();
		
		public function new() 
		{
			super();
		}
		
		override public function create():Void
		{
			
			FlxG.mouse.visible = false;
		
			title = new FlxSprite(0,0,"assets/images/titlescreen.png");
			howtoplay = new FlxSprite(0,0,"assets/images/howtoplay.png");
			
			ggj = new FlxSprite(0,0,"assets/images/ggj.png");
			bgs = new FlxSprite(0,0,"assets/images/bgs.png");
			
			add(howtoplay);
			add(title);
			add(bgs);
			add(ggj);
			
		}
		
		override public function update(elapsed:Float):Void
		{
			super.update(elapsed);
			
			_gamePads[0] = FlxG.gamepads.getByID(0);
//			_gamePads[1] = FlxG.gamepads.getByID(1);

			if(ggj.visible)
			{
				introTimer += elapsed;
				if(introTimer >= 3)
				{
					introTimer = 0;
					ggj.visible = false;
				}
			}
			else if(bgs.visible)
			{
				introTimer += elapsed;
				if(introTimer >= 3)
				{
					introTimer = 0;
					bgs.visible = false;
					FlxG.sound.playMusic("assets/music/title.ogg", 0.7, true);
					
				}
			}

			if (_gamePads[0] != null)
				gamepadControls(0);
			controls();

			if(FlxG.keys.anyJustPressed([ESCAPE]))
				System.exit(0);
			
		}
		
		
		private function gamepadControls(P:Int):Void
		{
			if(_gamePads[P].justPressed.A || _gamePads[P].justPressed.B || _gamePads[P].justPressed.X || _gamePads[P].justPressed.Y)
				nextScreen();
			
		}
		
		private function controls():Void
		{
			if(FlxG.keys.anyJustPressed([ ENTER, SPACE,J,K,Z,X]))
				nextScreen();

		}
		
		private function nextScreen():Void
		{
			if(ggj.visible || bgs.visible)
			{
				ggj.visible = false;
				bgs.visible = false;
				FlxG.sound.playMusic("assets/music/title.ogg", 0.7, true);
				
				return;
			}
			
			if(title.visible)
				title.visible = false;
			else
			{
				FlxG.sound.music.stop();
				FlxG.switchState(new PlayState());				
			}
		}
	}
	