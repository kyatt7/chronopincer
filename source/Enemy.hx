package ;

	import flixel.FlxSprite;
	import flixel.FlxObject;
	
	class Enemy extends FlxSprite
	{
		public var type:Int = 1;
		
		public var readyToFire:Bool = false;
		public var readyTimer:Float = 0.5;
		
		public var enemyHealth:Int = 20;
		
		public var altFire:Bool = false;
		
		private var THRESHOLD:Float = 1;
		
		public function new(X:Float, Y:Float, T:Int)
		{
			super(X*30, Y);
			
			loadGraphic("assets/images/enemy"+ T +".png", true, 30, 30);
			animation.add("idle", [0,1,2,3,3,3], 6, true);
			animation.add("hit", [4,3], 16, false);
			
			height = 24;
			width = 24;
			offset.x = 3;
			offset.y = 3;
			
			type = T;
			
			if(type == 1)
			{
				if(X == 0)
					velocity.x = 30;
				if(X == 1)
					velocity.x = 20;
				if(X == 4)
					velocity.x = -20;
				if(X == 5)
					velocity.x = -30;

				velocity.y = 100;
				acceleration.y = -50;
				enemyHealth = 30;
				
//				THRESHOLD = 0.5;
			}		
			if(type == 2)
			{
				
				enemyHealth = 40;
				velocity.y = 20;
			}
			
			animation.play("idle");
		}
		
		override public function update(elapsed:Float):Void
		{
			//
			
			if(y < -130 || y > 450 || x > 250 || x < -50)
				kill();
			
			readyTimer += elapsed;
			if(readyTimer > THRESHOLD)
			{
				readyToFire = true;
			}
			
			super.update(elapsed);
		}
		
		
		override public function draw():Void
		{
			super.draw();
		}
		
	}
