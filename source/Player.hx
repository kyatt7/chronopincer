package;

	import flixel.FlxG;
	import flixel.group.FlxGroup;
	import flixel.math.FlxPoint;
	import flixel.FlxObject;
	import flixel.FlxSprite;
	
	class Player extends FlxSprite
	{
		
		private var speed:Int = 75;
		
		private var clock:FlxSprite;
		private var leftClawSprite:FlxSprite;
		private var rightClawSprite:FlxSprite;
		
		public var leftClawBirth:Int = 0;
		public var rightClawBirth:Int = 0;
		
		public var type:Int = 0;
		
		public var leftClaw:Float = 16;
		public var rightClaw:Float = 16;
		public var prevLeftClaw:Float = 16;
		public var prevRightClaw:Float = 16;
		
		public var leftClawOrigin:FlxPoint;
		public var rightClawOrigin:FlxPoint;
		
		public var hurtTimer:Float = 0;
		public var blinkTimer:Float = 0;
		
		public var i:Int = 0;
		
		public function new(X:Float, Y:Float)
		{
			
			super(X, Y);
			
			loadGraphic("assets/images/playersprite.png", true, 50, 50);
			
			animation.add("idle", [0], 8, true);
			animation.add("walk", [0,0,1,2,3,3,4,5], 12, true);
			
			animation.play("player");
			
			leftClawOrigin = new FlxPoint(x,y);
			rightClawOrigin = new FlxPoint(x,y);
			
			
			clock = new FlxSprite(X,Y);
			clock.loadGraphic("assets/images/clocksprite.png",true,50,50);
			clock.animation.add("glow",[0,1,2,3],4,true);
			clock.animation.play("glow");

			leftClawSprite = new FlxSprite(X,Y);
			leftClawSprite.loadGraphic("assets/images/clawsprite1.png",true,50,50);
			for(i in 0...5)
				leftClawSprite.animation.add(""+i,[i],true);
			leftClawSprite.animation.add("ghost",[5,6],4,true);
			leftClawSprite.animation.play(""+4);
			
			rightClawSprite = new FlxSprite(X,Y);
			rightClawSprite.loadGraphic("assets/images/clawsprite2.png",true,50,50);
			for(i in 0...5)
				rightClawSprite.animation.add(""+i,[i],true);
			rightClawSprite.animation.add("ghost",[5,6],4,true);
			rightClawSprite.animation.play(""+4);
			
			height = 2;
			width = 2;
			offset.x = 24;
			offset.y = 34;
			
		}
		
		
		override public function update(elapsed:Float):Void
		{
			prevLeftClaw = leftClaw;
			prevRightClaw = rightClaw;
			
			if(leftClaw < 15)
				leftClaw += elapsed;
			if(rightClaw < 15)
				rightClaw += elapsed;
			
			if(hurtTimer > 0)
			{
				hurtTimer -= elapsed;
				blinkTimer += elapsed;
				if(blinkTimer > 0.07)
				{
					visible = !visible;
					blinkTimer = 0;
				}							
			}
			else
			{
				visible = true;
			}
			
			if(velocity.y != 0 && velocity.x != 0)
			{
				velocity.x = velocity.x * 0.75;
				velocity.y = velocity.y * 0.75;
			}

			if(velocity.x != 0 || velocity.y != 0)
				animation.play("walk");
			else
				animation.play("idle");
			
			if(type == 0)
			{
				if(leftClaw >= 15)
					leftClawSprite.animation.play("4");
				else if(leftClaw > 7.5)
					leftClawSprite.animation.play("3");
				else if(leftClaw > 5)
					leftClawSprite.animation.play("2");
				else if(leftClaw > 2.5)
					leftClawSprite.animation.play("1");
				else
					leftClawSprite.animation.play("0");
			
				if(rightClaw >= 15)
					rightClawSprite.animation.play("4");
				else if(rightClaw > 7.5)
					rightClawSprite.animation.play("3");
				else if(rightClaw > 5)
					rightClawSprite.animation.play("2");
				else if(rightClaw > 2.5)
					rightClawSprite.animation.play("1");
				else
					rightClawSprite.animation.play("0");				
			}
			
			clock.update(elapsed);
			leftClawSprite.update(elapsed);
			rightClawSprite.update(elapsed);
			super.update(elapsed);
			
		}
		
		
		public function moveX(N:Int):Void
		{
			velocity.x = speed * N;
			if(velocity.x > 0)
				flipX = false;
			if(velocity.x < 0)
				flipX = true;
		}

		public function moveY(N:Int):Void
		{
			velocity.y = speed * N;			
		}
		
		override public function draw():Void
		{
			//playerTrail.draw();
			if(type == 0)
				super.draw();
			
			if(type == 0 || type == 1)
			{
				leftClawSprite.x = x-offset.x;
				leftClawSprite.y = y-offset.y;				
				leftClawSprite.draw();
			}

			if(type == 0 || type == 2)
			{
				rightClawSprite.x = x-offset.x;
				rightClawSprite.y = y-offset.y;				
				rightClawSprite.draw();
			}

			if(type == 0)
			{
				clock.x = x-offset.x;
				clock.y = y-offset.y;				
				clock.draw();
			}
		}
		
		
	}
