package ;

	import flixel.FlxSprite;
	import flixel.FlxObject;
	
	class EnemyBullet extends FlxSprite
	{
		public var type:Int = 1;
		private var speed:Int = 50;
		
		public function new(X:Float, Y:Float, T:Int, D:Int)
		{
			super(X, Y);
			
			loadGraphic("assets/images/enemybullet.png", true, 12, 12);
			animation.add("bullet1", [0], 2, true);
			animation.add("bullet2", [1], 2, true);
			animation.add("bullet3", [2], 2, true);
			
			height = 8;
			width = 8;
			offset.x = 2;
			offset.y = 2;
			
			type = T;		
			animation.play("bullet"+type);
			
			velocity.x = Math.cos(D/(180/3.1419))*speed;
			velocity.y = Math.sin(D/(180/3.1419))*speed;
			
		}
		
		public function recycle(X:Float, Y:Float, T:Int, D:Int)
		{
			revive();
			x = X;
			y = Y;

			type = T;
			animation.play("bullet"+type);
			
			velocity.x = Math.cos(D/(180/3.1419))*speed;
			velocity.y = Math.sin(D/(180/3.1419))*speed;
		}
		
		override public function update(elapsed:Float):Void
		{
			//
			
			if(y < -30 || y > 400 || x > 200 || x < -10)
				kill();
			
			super.update(elapsed);
		}
		
		
		override public function draw():Void
		{
			super.draw();
		}
		
	}
