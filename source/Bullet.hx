package ;

	import flixel.FlxSprite;
	import flixel.FlxObject;
	
	class Bullet extends FlxSprite
	{
		public var type:Int = 1;
		
		public function new(X:Float, Y:Float, T:Int)
		{
			super(X, Y);
			
			loadGraphic("assets/images/bulletsprite.png", true, 6, 10);
			animation.add("bullet1", [0], 2, true);
			animation.add("bullet2", [1], 2, true);
			animation.add("bullet3", [2], 2, true);
			
			height = 8;
			width = 4;
			offset.x = 1;
			offset.y = 1;
			
			type = T;		
			if(type == 2)
				velocity.y = -400;
			else
				velocity.y = -300;
			
			animation.play("bullet"+type);
		}
		
		public function recycle(X:Float, Y:Float, T:Int)
		{
			revive();
			x = X;
			y = Y;
			
			type = T;		
			if(type == 2)
				velocity.y = -400;
			else
				velocity.y = -300;
			
			animation.play("bullet"+type);
		}
		
		override public function update(elapsed:Float):Void
		{
			//
			
			if(y < -30)
				kill();
			
			super.update(elapsed);
		}
		
		
		override public function draw():Void
		{
			super.draw();
		}
		
	}
