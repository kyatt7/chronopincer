package; 

	import flixel.FlxSprite;
	import flixel.FlxObject;
	import flixel.text.FlxText;
	import flixel.FlxState;
	import flixel.FlxG;
	import flixel.group.FlxGroup;
//	import flixel.group.FlxTypedGroup;
	import flixel.util.FlxSort;
	import flixel.math.FlxMath;
	import flixel.util.FlxTimer;
	import flixel.math.FlxRandom;
	import flixel.tile.FlxTilemap;
	
	import flixel.input.gamepad.FlxGamepad;
	import flixel.input.gamepad.FlxGamepadInputID;
	
	class PlayState extends FlxState
	{
		
		private var _gamePads:Array<FlxGamepad> = new Array<FlxGamepad>();
		
		private var frameIndex:Int = 0;
		
		private var gameOverTimer:Float = 0;
		private var continueTimer:Float = 0;
		
		private var spawnTimer:Float = 0;
		private var spawnIndex:Int = 0;
		
		private var continueCounter:Int = 0;
		
		private var continueText:FlxText;
		private var continueScreen:FlxSprite;
		private var gameOverScreen:FlxSprite;
		private var endingScreen:FlxSprite;
		private var youWin:Bool = false;
		
		private var enemyMap:Array<Array<Int>> = 
			[
				[0,0,0,0,0,0],
				[0,2,0,0,2,0],
				[0,0,0,0,0,0],
				[2,2,0,0,2,2],
				[0,0,0,0,0,0],
				[0,0,2,2,0,0],
				[0,0,0,0,0,0],
				[0,1,0,0,1,0],
				[1,0,0,0,0,1],
				[0,0,0,0,0,0],
				[0,0,2,2,0,0],
				[0,2,0,0,2,0],
				[2,0,0,0,0,2],
				[0,0,0,0,0,0],
				[0,0,1,1,0,0],
				[0,1,0,0,1,0],
				[0,0,0,0,0,0],
				[1,0,0,0,0,1],
				[1,0,2,2,0,1],
				[1,0,0,0,0,1],
				[0,0,0,0,0,0],
				[1,0,0,0,2,2],
				[1,0,0,0,0,0],
				[1,0,0,0,0,0],
				[1,0,1,0,0,0],
				[0,0,0,2,0,0],
				[0,0,0,0,0,1],
				[0,2,2,0,0,1],
				[0,0,0,0,0,1],
				[0,0,0,1,0,1],
				[0,0,0,0,0,0],
				[0,0,0,0,0,0],
				[0,1,0,0,1,0],
				[0,1,0,0,1,0],
				[0,0,0,0,0,0],
				[2,2,2,2,2,2],
				[0,0,0,0,0,0],
				[0,2,2,2,2,0],
				[2,0,1,1,0,2],
				[0,0,0,0,0,0],
				[1,0,0,0,0,0],
				[0,1,0,0,0,0],
				[0,0,1,0,0,0],
				[0,0,0,1,0,0],
				[0,0,0,0,1,0],
				[0,0,0,0,0,1],
				[0,2,0,0,2,0],
				[0,0,0,0,0,0],
				[0,0,0,0,1,1],
				[0,0,0,1,1,0],
				[0,0,1,1,0,0],
				[0,1,1,0,0,0],
				[1,1,0,0,0,0],
				[0,0,2,2,0,0],
				[0,2,0,0,2,0],
				[0,0,2,2,0,0],
				[0,0,0,0,0,0],
				[1,0,1,0,1,0],
				[0,1,0,1,0,1],
				[0,0,0,0,0,0],
				[0,0,0,0,0,0],
				[0,0,0,0,0,0],
				[0,0,0,0,0,0],
				[0,0,0,0,0,0],
				[0,0,0,0,0,0],
			];
		
		private var player:Player;
		private var bullets:FlxTypedGroup<Bullet> = new FlxTypedGroup<Bullet>();
		private var enemyBullets:FlxTypedGroup<EnemyBullet> = new FlxTypedGroup<EnemyBullet>();
		private var ghostGroup:FlxTypedGroup<Ghost> = new FlxTypedGroup<Ghost>();

		private var ghosts:Array<Ghost> = new Array<Ghost>();
		
		private var memory:Array<Array<Array<Bool>>> = new Array<Array<Array<Bool>>>();
		
		//ghost# -> frame# -> L, R, U, D, shoot
		
		private var enemies:FlxTypedGroup<Enemy> = new FlxTypedGroup<Enemy>();
		
		private var background1:FlxSprite;
		
		
		private var background2:FlxSprite;
		private var background3:FlxSprite;
		
		private var bulletCooldown:Float = 0;
		private var ghostCooldown:Array<Float> = new Array<Float>();
		
		private var credits:Int = 5;
		private var creditText:FlxText;
		private var score:Int = 0;
		private var scoreText:FlxText;


		private var introText:FlxText;
		private var introTimer:Float = 5;
		
		
		private var i:Int = 0;
		
		public function new() 
		{
			super();
		}
		
		override public function create():Void
		{
			FlxG.mouse.visible = false;
			
			player = new Player(110,280);
			
			FlxG.sound.playMusic("assets/music/music.ogg", 0.7, true);
			
			background1 = new FlxSprite(0,-300,"assets/images/background1.png");
			background2 = new FlxSprite(0,-300,"assets/images/background2.png");
			background3 = new FlxSprite(0,-300,"assets/images/background3.png");
			
			background1.velocity.y = 8;
			background2.velocity.y = 12;
			background3.velocity.y = 16;
			
			continueScreen = new FlxSprite(0,0,"assets/images/continue.png");
			gameOverScreen = new FlxSprite(0,0,"assets/images/gameover.png");
			endingScreen = new FlxSprite(0,0,"assets/images/ending.png");
			continueScreen.visible = false;
			gameOverScreen.visible = false;
			endingScreen.visible = false;
			
			continueText = new FlxText(70,120);
			continueText.color = 0xff000000;
			continueText.size = 64;
			continueText.visible = false;
			
			creditText = new FlxText(10,300,160,"CREDITS: "+credits);
			creditText.color = 0xff2657bc;
			creditText.setBorderStyle(OUTLINE,0xffffffff,1,1);
			creditText.alignment = RIGHT;

			introText = new FlxText(10,150,160,"EGYPT: 42 BC");
			introText.color = 0xff000000;
			introText.setBorderStyle(OUTLINE,0xffffffff,1,1);
			introText.alignment = CENTER;

			scoreText = new FlxText(10,300,160,"SCORE: "+score);
			scoreText.color = 0xff2657bc;
			scoreText.setBorderStyle(OUTLINE,0xffffffff,1,1);
			scoreText.alignment = LEFT;
			
			// for(i in 0...Reg.ghosts)
			// {
			// 	memory[i] = Reg.memory[i];
			// }
			//
			// for(i in 0...Reg.ghosts)
			// {
			// 	ghosts[i] = new Ghost(110,280);
			// }

			memory[0] = new Array<Array<Bool>>();
			memory[1] = new Array<Array<Bool>>();
			
			for(i in 0...ghosts.length)
				ghostGroup.add(ghosts[i]);
			
			add(background1);
			add(background2);
			add(background3);
			add(scoreText);
			add(creditText);
			add(enemies);
			add(player);
			add(ghostGroup);
			add(enemyBullets);
			add(bullets);
			add(introText);
			add(continueScreen);
			add(continueText);
			add(gameOverScreen);
			add(endingScreen);
			
		}
		
		override public function update(elapsed:Float):Void
		{
			super.update(elapsed);
			
			_gamePads[0] = FlxG.gamepads.getByID(0);
			_gamePads[1] = FlxG.gamepads.getByID(1);
			
			if(introTimer > 0)
			{
				introTimer -= elapsed;
				if(introTimer <= 0)
				{
					introText.visible = false;
				}
			}
			
			if(gameOverTimer == 0 && continueTimer == 0)
			{
				FlxG.overlap(bullets, enemies, shootEnemy);
				FlxG.overlap(enemyBullets, player, shootPlayer);
				FlxG.overlap(enemies, player, hitPlayer);				
			}
			
			enemies.forEach(function(en:Enemy)
			{
				if(en.isOnScreen(FlxG.camera))
				{
					if(en.readyToFire)
					{
						en.altFire = !en.altFire;
						en.readyTimer = 0;
						en.readyToFire = false;
						if(en.type == 1) //pyramid
						{
							addEnemyBullet(en.x+8, en.y+8, en.type, 70);
							addEnemyBullet(en.x+8, en.y+8, en.type, 80);
							addEnemyBullet(en.x+8, en.y+8, en.type, 90);
							addEnemyBullet(en.x+8, en.y+8, en.type, 100);
							addEnemyBullet(en.x+8, en.y+8, en.type, 110);
						}
						else if(en.type == 2)
						{
							if(en.altFire)
							{
								addEnemyBullet(en.x+8, en.y+8, en.type, 90);
								addEnemyBullet(en.x+8, en.y+8, en.type, 180);
								addEnemyBullet(en.x+8, en.y+8, en.type, 270);
								addEnemyBullet(en.x+8, en.y+8, en.type, 360);								
							}
							else
							{
								addEnemyBullet(en.x+8, en.y+8, en.type, 90-45);
								addEnemyBullet(en.x+8, en.y+8, en.type, 180-45);
								addEnemyBullet(en.x+8, en.y+8, en.type, 270-45);
								addEnemyBullet(en.x+8, en.y+8, en.type, 360-45);	
							}
						}
					}
				}
			});
			
			
			if(gameOverTimer > 0)
			{
				player.visible = false;
				
				continueScreen.visible = false;
				continueText.visible = false;
				gameOverScreen.visible = true;
				if(youWin)
					endingScreen.visible = true;
				gameOverTimer += elapsed;
				if(gameOverTimer >= 5)
				{ 
					FlxG.sound.music.stop();
					FlxG.switchState(new TitleState());
				}
			}
			else if(continueTimer > 0)
			{
				if(credits == 0)
					gameOverTimer = 0.01;
				player.visible = false;
				continueScreen.visible = true;
				continueText.visible = true;
				continueTimer += elapsed;
				continueText.text = ""+continueCounter;
				if(continueTimer >= 0.66)
				{
					continueCounter--;
					continueText.text = ""+continueCounter;
					if(continueCounter <= 0)
					{
						gameOverTimer = 0.01;
					}
					continueTimer = 0.01;
				}
			}
			
			spawnTimer += elapsed;
			if(spawnTimer >= 2 && gameOverTimer == 0)
			{
				for(i in 0...6)
				{
					if(enemyMap[spawnIndex][i] != 0)
					{
						enemies.add(new Enemy(i,-20,enemyMap[spawnIndex][i]));
					}
				}
				spawnTimer = 0;
				spawnIndex++;
				if(spawnIndex >= enemyMap.length)
				{
					gameOverTimer = 0.1;
					youWin = true;
				}
			}
			
			
			
			if(player.x >= 170)
				player.x = 170;
			if(player.x <= 8)
				player.x = 8;
			if(player.y >= 310)
				player.y = 310;
			if(player.y <= 100)
				player.y = 100;
			
			for(i in 0...ghosts.length)
			{
				if(ghosts[i].x >= 170)
					ghosts[i].x = 170;
				if(ghosts[i].x <= 8)
					ghosts[i].x = 8;
				if(ghosts[i].y >= 310)
					ghosts[i].y = 310;
				if(ghosts[i].y <= 100)
					ghosts[i].y = 100;
			}
			
			if(background1.y > 0)
				background1.y -= 320;
			if(background2.y > 0)
				background2.y -= 320;
			if(background3.y > 0)
				background3.y -= 320;
			
			player.velocity.x = 0;
			player.velocity.y = 0;
			
			for(i in 0...ghosts.length)
			{
				ghosts[i].velocity.x = 0;
				ghosts[i].velocity.y = 0;
			}
			
			// if(player.leftClaw >= 10 && player.prevLeftClaw < 10)
			// {
			// 	player.leftClawOrigin.x = player.x;
			// 	player.leftClawOrigin.y = player.y;
			// }

			// if(player.rightClaw >= 10 && player.prevRightClaw < 10)
			// {
			// 	player.rightClawOrigin.x = player.x;
			// 	player.rightClawOrigin.y = player.y;
			// }

			if(player.leftClaw >= 15)
				memory[0].push( [false, false, false, false, false] );

			if(player.rightClaw >= 15)
				memory[1].push( [false, false, false, false, false] );
			
			if (_gamePads[0] != null)
				gamepadControls(0);
			controls();

			for(i in 0...ghosts.length)
			{
				ghostControls(i);
			}

			if(FlxG.keys.anyJustPressed([X]))
			{
				hitCrab();
			}

			if(FlxG.keys.anyJustPressed([R]))
			{
				FlxG.sound.music.stop();
				FlxG.switchState(new PlayState());
			}
			
			if(FlxG.keys.anyJustPressed([ESCAPE]))
			{
				FlxG.sound.music.stop();
				FlxG.switchState(new TitleState());
			}

			frameIndex++;
		}
		
		private function shootEnemy(B:Bullet, E:Enemy):Void
		{
			E.enemyHealth -= (4-B.type);
			E.animation.play("hit");
			if(E.enemyHealth <= 0)
			{
				E.kill();
				score += 1000 * (3-E.type);
				scoreText.text = "SCORE: "+score;
			}
			B.kill();
			
		}

		private function shootPlayer(B:EnemyBullet, P:Player):Void
		{
			B.kill();
			
			hitCrab();
		}
		
		private function hitPlayer(E:Enemy, P:Player):Void
		{
			hitCrab();
		}
		
		private function ghostControls(P:Int):Void
		{
			if(memory[P+2].length == 0)
			{
				ghosts[P].isActive = false;
				return;
			}
			
			var tempMemory:Array<Bool> = memory[P+2].pop();

			if(tempMemory[0])
				ghosts[P].moveX(1);
			if(tempMemory[1])
				ghosts[P].moveX(-1);
			if(tempMemory[2])
				ghosts[P].moveY(1);
			if(tempMemory[3])
				ghosts[P].moveY(-1);
			if(tempMemory[4])
			{
				ghostCooldown[P] -= FlxG.elapsed;
				if(ghostCooldown[P] <= 0)
				{
					if(ghosts[P].type == 1)
						addBullet(ghosts[P].x-17,ghosts[P].y-30, 1);
					if(ghosts[P].type == 2)
						addBullet(ghosts[P].x+13,ghosts[P].y-30, 1);
					ghostCooldown[P] = 0.1;
				}
			}
		}
		
		private function gamepadControls(P:Int):Void
		{
			if(_gamePads[P].pressed.DPAD_RIGHT || _gamePads[P].analog.value.LEFT_STICK_X > 0.55 || _gamePads[P].analog.value.RIGHT_STICK_X > 0.55)
				pressRight(P);
			else if (_gamePads[P].pressed.DPAD_LEFT || _gamePads[P].analog.value.LEFT_STICK_X < -0.55 || _gamePads[P].analog.value.RIGHT_STICK_X < -0.55)
				pressLeft(P);
					
			if(_gamePads[P].pressed.DPAD_DOWN || _gamePads[P].analog.value.LEFT_STICK_Y > 0.55 || _gamePads[P].analog.value.RIGHT_STICK_Y > 0.55)
				pressDown(P);
			
			else if(_gamePads[P].pressed.DPAD_UP || _gamePads[P].analog.value.LEFT_STICK_Y < -0.55 || _gamePads[P].analog.value.RIGHT_STICK_Y < -0.55) 
				pressUp(P);
			
			if(_gamePads[P].pressed.A || _gamePads[P].pressed.X || _gamePads[P].pressed.LEFT_TRIGGER || _gamePads[P].pressed.RIGHT_TRIGGER )
				pressA(P);

			if(_gamePads[P].justPressed.A || _gamePads[P].justPressed.X || _gamePads[P].justPressed.LEFT_TRIGGER || _gamePads[P].justPressed.RIGHT_TRIGGER )
				justPressA(P);

			if(_gamePads[P].justPressed.B || _gamePads[P].justPressed.Y || _gamePads[P].justPressed.LEFT_SHOULDER || _gamePads[P].justPressed.RIGHT_SHOULDER)
				pressB(P);
			
			if(_gamePads[P].justPressed.START)
			{
				credits++;
				creditText.text = "CREDITS: "+credits;
			}
		}
		
		private function controls():Void
		{
			if(FlxG.keys.anyPressed([RIGHT,D]))
				pressRight(0);
			else if (FlxG.keys.anyPressed([LEFT,A]))
				pressLeft(0);

			if(FlxG.keys.anyPressed([DOWN,S]))
				pressDown(0);
			else if(FlxG.keys.anyPressed([UP, W]))
				pressUp(0);

			if(FlxG.keys.anyPressed([K,Z,SPACE]))
				pressA(0);
			
			if(FlxG.keys.anyJustPressed([K,Z,SPACE]))
				justPressA(0);

			if(FlxG.keys.anyJustPressed([J,X]))
				pressB(0);
			
			if(FlxG.keys.anyJustPressed([ONE]))
			{
				credits++;
				creditText.text = "CREDITS: "+credits;
			}
			
		}
		
		private function justPressA(N:Int):Void
		{
			if(continueTimer > 0 && gameOverTimer == 0)
			{
				credits--;
				creditText.text = "CREDITS: "+credits;
				continueTimer = 0;
				continueScreen.visible = false;
				continueText.visible = false;
				player.visible = true;
				player.leftClaw = 15;
				player.rightClaw = 15;
				player.hurtTimer = 2;
				return;
			}
		}
		
		private function hitCrab():Void
		{
			if(player.hurtTimer > 0)
				return;
			
			if(player.leftClaw < 15 && player.rightClaw < 15)
			{
				continueCounter = 9;
				continueTimer = 0.01;
				player.visible = false;
				return;
			}
			
			FlxG.sound.play("assets/sounds/hit.ogg", 0.8, false, false);
			
			player.hurtTimer = 2;
			
			var newMemory:Int = memory.length-2;
			memory[memory.length] = [[false,false,false,false,false]];
			
			if(player.leftClawBirth <= player.rightClawBirth)
			{
				if(player.leftClaw >= 15)
				{
					removeLeftClaw(newMemory);
				}
				else if(player.rightClaw >= 15)
				{
					removeRightClaw(newMemory);
				}
			}
			else
			{
				if(player.rightClaw >= 15)
				{
					removeRightClaw(newMemory);
				}
				else if(player.leftClaw >= 15)
				{
					removeLeftClaw(newMemory);
				}
			}
		}
		
		private function removeLeftClaw(N):Void
		{
			ghosts[N] = new Ghost(player.x, player.y, 1);
			ghostGroup.add(ghosts[N]);
			
			player.leftClaw = 0;
			memory[N+2] = memory[0];
			memory[0] = [[false,false,false,false,false]];
			ghosts[N].isActive = true;
			player.leftClawOrigin.x = player.x;
			player.leftClawOrigin.y = player.y;
			player.leftClawBirth = frameIndex;
		}
		
		private function removeRightClaw(N):Void
		{

			ghosts[N] = new Ghost(player.x, player.y, 2);
			ghostGroup.add(ghosts[N]);
			
			player.rightClaw = 0;
			memory[N+2] = memory[1];
			memory[1] = [[false,false,false,false,false]];
			ghosts[N].isActive = true;
			player.rightClawOrigin.x = player.x;
			player.rightClawOrigin.y = player.y;
			player.rightClawBirth = frameIndex;

		}
		

		private function pressLeft(P:Int):Void
		{
			if(gameOverTimer > 0 || continueTimer > 0)
				return;
			
			if(player.leftClaw >= 15)
				memory[0][memory[0].length-1][0] = true;
			if(player.rightClaw >= 15)
				memory[1][memory[1].length-1][0] = true;

			player.moveX(-1);
		}

		private function pressRight(P:Int):Void
		{
			if(gameOverTimer > 0 || continueTimer > 0)
				return;

			if(player.leftClaw >= 15)
				memory[0][memory[0].length-1][1] = true;
			if(player.rightClaw >= 15)
				memory[1][memory[1].length-1][1] = true;

			player.moveX(1);			
		}

		private function pressUp(P:Int):Void
		{
			if(gameOverTimer > 0 || continueTimer > 0)
				return;

			if(player.leftClaw >= 15)
				memory[0][memory[0].length-1][2] = true;
			if(player.rightClaw >= 15)
				memory[1][memory[1].length-1][2] = true;

			player.moveY(-1);			
		}

		private function pressDown(P:Int):Void
		{
			if(gameOverTimer > 0 || continueTimer > 0)
				return;

			if(player.leftClaw >= 15)
				memory[0][memory[0].length-1][3] = true;
			if(player.rightClaw >= 15)
				memory[1][memory[1].length-1][3] = true;

			player.moveY(1);
		}


		private function pressA(P:Int):Void
		{
			if(gameOverTimer > 0 || continueTimer > 0)
				return;

			if(player.leftClaw >= 10)
				memory[0][memory[0].length-1][4] = (player.leftClaw >= 10);
			
			if(player.rightClaw >= 10)
				memory[1][memory[1].length-1][4] = (player.rightClaw >= 10);
			
			bulletCooldown -= FlxG.elapsed;
			if(bulletCooldown <= 0)
			{
				FlxG.sound.play("assets/sounds/sfx1.ogg", 0.4, false, false);
				
				if(player.leftClaw >= 15)
					addBullet(player.x-17, player.y-30, 1);
				else if(player.leftClaw >= 7.5)
					addBullet(player.x-17, player.y-30, 3);


				if(player.rightClaw >= 15)
					addBullet(player.x+15, player.y-30, 1);
				else if(player.rightClaw >= 7.5)
					addBullet(player.x+15, player.y-30, 3);

				addBullet(player.x-6,player.y-25, 2);
				addBullet(player.x+4,player.y-25, 2);

				bulletCooldown = 0.1;
			}
		}

		private function addBullet(X:Float, Y:Float, T:Int):Void
		{
			if(bullets.countDead() > 0)
				cast(bullets.getFirstDead(), Bullet).recycle(X, Y, T);
			else
				bullets.add(new Bullet(X, Y, T));
		}


		private function addEnemyBullet(X:Float, Y:Float, T:Int, D:Int):Void
		{
			if(enemyBullets.countDead() > 0)
				cast(enemyBullets.getFirstDead(), EnemyBullet).recycle(X, Y, T, D);
			else
				enemyBullets.add(new EnemyBullet(X, Y, T, D));
		}


		private function pressB(P:Int):Void
		{
			if(gameOverTimer > 0)
				return;

			if(continueTimer > 0)
			{
				credits--;
				creditText.text = "CREDITS: "+credits;
				continueTimer = 0;
				continueScreen.visible = false;
				continueText.visible = false;
				player.visible = true;
				player.leftClaw = 15;
				player.rightClaw = 15;
				player.hurtTimer = 2;
				return;
			}

			hitCrab();
		}

		
		private function getItem(P:Player, I:Item):Void
		{
			I.kill();
		}

		
	}