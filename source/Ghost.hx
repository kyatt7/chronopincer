package;

	import flixel.FlxG;
	import flixel.group.FlxGroup;
	import flixel.FlxObject;
	import flixel.FlxSprite;
	
	class Ghost extends Player
	{
		public var isActive:Bool = true;
		
		public function new(X:Float, Y:Float, T:Int)
		{
			
			super(X, Y);
			
			type = T;
			
			leftClawSprite.animation.play("ghost");
			rightClawSprite.animation.play("ghost");
			
		}
		
		
		override public function update(elapsed:Float):Void
		{
			
			if(!isActive)
			{
				clock.alpha -= elapsed/3;
				leftClawSprite.alpha -= elapsed/3;
				rightClawSprite.alpha -= elapsed/3;
				alpha -= elapsed/3;
			}

			clock.update(elapsed);
			leftClawSprite.update(elapsed);
			rightClawSprite.update(elapsed);
			super.update(elapsed);
			
		}
		
		
		// public function moveX(N:Int):Void
		// {
		// 	velocity.x = speed * N;
		// }
		//
		// public function moveY(N:Int):Void
		// {
		// 	velocity.y = speed * N;
		// }
		
		override public function draw():Void
		{
			//playerTrail.draw();
			super.draw();
		}
		
		
	}
